function isMobileDevice() {
  var ua = navigator.userAgent;
  return ua.match(/Android/i)
    || ua.match(/webOS/i)
    || ua.match(/iPhone/i)
    || ua.match(/iPad/i)
    || ua.match(/iPod/i)
    || ua.match(/BlackBerry/i)
    || ua.match(/Windows Phone/i);
};

if (!isMobileDevice()) {
  if (navigator.userAgent.includes('Firefox')) {
    document.write('<p id="hint">Hint: Press <mark id="keystroke">Alt</mark><mark id="keystroke">Shift</mark>-<mark id="keystroke">m</mark>/<mark id="keystroke">p</mark>/<mark id="keystroke">l</mark> to switch between pages</p>');
  } else {
    document.write('<p id="hint">Hint: Press <mark id="keystroke">Alt</mark>-<mark id="keystroke">m</mark>/<mark id="keystroke">p</mark>/<mark id="keystroke">l</mark> to switch between pages</p>');
  }
}
