<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/links.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Fira+Sans&display=swap">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Crimson+Text&display=swap">
    <link rel="icon" type="image/svg" href="logo/logo_trans_distressed.svg"/>
    <title>Agatha Rose • Links</title>
  </head>
  <body>
    <div id="content">
      <nav>
        <img src="logo/logo_trans_distressed.svg" alt="Logo">
        <a href="/" accesskey="m"><i class="fas fa-moon"></i> Main</a>
        <a href="/projects" accesskey="p"><i class="fas fa-book"></i> Projects</a>
        <a class="active" href="/links" accesskey="l"><i class="fas fa-link"></i> Links<br>•</a>
      </nav>
      <h1 id="main-header">Find me</h1>
      <div class="links">
        <div class="link">
          <h1><i class="fab fa-discord fa-lg"></i> Discord</h1>
          <!-- gets discord username from file -->
          <p><i class="fas fa-cat"></i> <?php
            $agapath = "aganame";
            $agafile = fopen($agapath, "r") or die("Unable to open file!");
            echo fread($agafile,filesize($agapath));
            fclose($agafile);
          ?></p>
        </div>
        <div class="link">
          <h1><a href="https://t.me/agahatt"><i class="text-icon fab fa-telegram fa-lg"></i> <span id="link-header">Telegram</span></a></h1>
        </div>
        <div class="link">
          <h1><a href="https://open.spotify.com/user/yezvm1by1eflx5hlj26f7mky4?si=gQZrT4oOQXa3NxKSShXHYA"><i class="text-icon fab fa-spotify fa-lg"></i> <span id="link-header">Spotify</span></a></h1>
        </div>
        <div class="link">
          <h1><a href="https://gitlab.com/agathasorceress"><i class="text-icon fab fa-gitlab fa-lg"></i> <span id="link-header">GitLab</span></a></h1>
        </div>
        <div class="link">
          <h1><a href="https://github.com/AgathaSorceress"><i class="text-icon fab fa-github fa-lg"></i> <span id="link-header">GitHub</span></a></h1>
        </div>
        <div class="link">
          <h1><a href="https://twitter.com/Agahat_"><i class="text-icon fab fa-twitter fa-lg"></i> <span id="link-header">Twitter</span></a></h1>
        </div>
        <div class="link">
          <h1><a rel="me" href="https://eldritch.cafe/@AgathaSorceress"><i class="text-icon fab fa-mastodon fa-lg"></i> <span id="link-header">Mastodon</span></a></h1>
        </div>
        <div class="link">
          <h1><a href="https://www.reddit.com/u/EvilDeaaaadd"><i class="text-icon fab fa-reddit fa-lg"></i> <span id="link-header">Reddit</span></a></h1>
        </div>
      </div>
    </div>
    <footer>
      <p>Made by Agatha Rose, with help of Julia Luna and StackOverflow <i class="fas fa-heart"></i></p>
    </footer>
  </body>
</html>
